init: docker-down-clear \
	api-clear frontend-clear cucumber-clear \
	docker-pull docker-build docker-up \
	api-init frontend-init cucumber-init
up: docker-up
down: docker-down
restart: down up
check: lint analyze validate-schema test test-e2e
lint: api-lint frontend-lint cucumber-lint
analyze: api-analyze
validate-schema: api-validate-schema
test: api-test api-fixtures frontend-test
test-unit: api-test-unit
test-functional: api-test-functional api-fixtures
test-smoke: api-fixtures cucumber-clear cucumber-smoke
test-e2e: api-fixtures cucumber-clear cucumber-e2e

update-deps: api-composer-update frontend-yarn-upgrade cucumber-yarn-upgrade restart

docker-up:
	docker-compose up -d

docker-down:
	docker-compose down --remove-orphans

docker-down-clear:
	docker-compose down -v --remove-orphans

docker-pull:
	- docker-compose pull

docker-build:
	DOCKER_BUILDKIT=1 COMPOSE_DOCKER_CLI_BUILD=1 docker-compose build --build-arg BUILDKIT_INLINE_CACHE=1 --pull

push-dev-cache:
	docker-compose push

api-clear:
	docker run --rm -v ${PWD}/api:/app -w /app alpine sh -c 'rm -rf var/cache/* var/log/* var/test/*'

api-init: api-permissions api-composer-install api-wait-db api-migrations api-fixtures

api-permissions:
	docker run --rm -v ${PWD}/api:/app -w /app alpine chmod 777 var/cache var/log var/test

api-composer-install:
	docker-compose run --rm api-php-cli composer install

api-composer-update:
	docker-compose run --rm api-php-cli composer update

api-wait-db:
	docker-compose run --rm api-php-cli wait-for-it api-postgres:5432 -t 30

api-migrations:
	docker-compose run --rm api-php-cli composer app migrations:migrate -- --no-interaction

api-fixtures:
	docker-compose run --rm api-php-cli composer app fixtures:load

api-check: api-validate-schema api-lint api-analyze api-test

api-validate-schema:
	docker-compose run --rm api-php-cli composer app orm:validate-schema

api-lint:
	docker-compose run --rm api-php-cli composer lint
	docker-compose run --rm api-php-cli composer phpcs

api-analyze:
	docker-compose run --rm api-php-cli composer psalm -- --no-diff

api-analyze-diff:
	docker-compose run --rm api-php-cli composer psalm

api-test:
	docker-compose run --rm api-php-cli composer test

api-test-coverage:
	docker-compose run --rm api-php-cli composer test-coverage

api-test-unit:
	docker-compose run --rm api-php-cli composer test -- --testsuite=unit

api-test-unit-coverage:
	docker-compose run --rm api-php-cli composer test-coverage -- --testsuite=unit

api-test-functional:
	docker-compose run --rm api-php-cli composer test -- --testsuite=functional

api-test-functional-coverage:
	docker-compose run --rm api-php-cli composer test-coverage -- --testsuite=functional

frontend-clear:
	docker run --rm -v ${PWD}/frontend:/app -w /app alpine sh -c 'rm -rf .ready build'

frontend-init: frontend-yarn-install frontend-ready

frontend-yarn-install:
	docker-compose run --rm frontend-node-cli yarn install

frontend-yarn-upgrade:
	docker-compose run --rm frontend-node-cli yarn upgrade

frontend-ready:
	docker run --rm -v ${PWD}/frontend:/app -w /app alpine touch .ready

frontend-check: frontend-lint frontend-test

frontend-lint:
	docker-compose run --rm frontend-node-cli yarn eslint
	docker-compose run --rm frontend-node-cli yarn stylelint

frontend-eslint-fix:
	docker-compose run --rm frontend-node-cli yarn eslint-fix

frontend-pretty:
	docker-compose run --rm frontend-node-cli yarn prettier

frontend-test:
	docker-compose run --rm frontend-node-cli yarn test --watchAll=false

frontend-test-watch:
	docker-compose run --rm frontend-node-cli yarn test

cucumber-clear:
	docker run --rm -v ${PWD}/cucumber:/app -w /app alpine sh -c 'rm -rf var/*'

cucumber-init: cucumber-yarn-install

cucumber-yarn-install:
	docker-compose run --rm cucumber-node-cli yarn install

cucumber-yarn-upgrade:
	docker-compose run --rm cucumber-node-cli yarn upgrade

cucumber-lint:
	docker-compose run --rm cucumber-node-cli yarn lint

cucumber-lint-fix:
	docker-compose run --rm cucumber-node-cli yarn lint-fix

cucumber-smoke:
	docker-compose run --rm cucumber-node-cli yarn smoke

cucumber-e2e:
	docker-compose run --rm cucumber-node-cli yarn e2e

build: build-gateway build-frontend build-api

build-gateway:
	DOCKER_BUILDKIT=1 docker --log-level=debug build --pull --build-arg BUILDKIT_INLINE_CACHE=1 \
    --cache-from ${REGISTRY}/gscore-gateway:cache \
    --tag ${REGISTRY}/gscore-gateway:cache \
    --tag ${REGISTRY}/gscore-gateway:${IMAGE_TAG} \
    --file gateway/docker/production/nginx/Dockerfile gateway/docker

build-frontend:
	DOCKER_BUILDKIT=1 docker --log-level=debug build --pull --build-arg BUILDKIT_INLINE_CACHE=1 \
    --target builder \
    --cache-from ${REGISTRY}/gscore-frontend:cache-builder \
    --tag ${REGISTRY}/gscore-frontend:cache-builder \
	--file frontend/docker/production/nginx/Dockerfile frontend

	DOCKER_BUILDKIT=1 docker --log-level=debug build --pull --build-arg BUILDKIT_INLINE_CACHE=1 \
    --cache-from ${REGISTRY}/gscore-frontend:cache-builder \
    --cache-from ${REGISTRY}/gscore-frontend:cache \
    --tag ${REGISTRY}/gscore-frontend:cache \
    --tag ${REGISTRY}/gscore-frontend:${IMAGE_TAG} \
	--file frontend/docker/production/nginx/Dockerfile frontend

build-api:
	DOCKER_BUILDKIT=1 docker --log-level=debug build --pull --build-arg BUILDKIT_INLINE_CACHE=1 \
    --cache-from ${REGISTRY}/gscore-api:cache \
    --tag ${REGISTRY}/gscore-api:cache \
	--tag ${REGISTRY}/gscore-api:${IMAGE_TAG} \
	--file api/docker/production/nginx/Dockerfile api

	DOCKER_BUILDKIT=1 docker --log-level=debug build --pull --build-arg BUILDKIT_INLINE_CACHE=1 \
	--target builder \
	--cache-from ${REGISTRY}/gscore-api-php-fpm:cache-builder \
	--tag ${REGISTRY}/gscore-api-php-fpm:cache-builder \
	--file api/docker/production/php-fpm/Dockerfile api

	DOCKER_BUILDKIT=1 docker --log-level=debug build --pull --build-arg BUILDKIT_INLINE_CACHE=1 \
	--cache-from ${REGISTRY}/gscore-api-php-fpm:cache-builder \
	--cache-from ${REGISTRY}/gscore-api-php-fpm:cache \
	--tag ${REGISTRY}/gscore-api-php-fpm:cache \
	--tag ${REGISTRY}/gscore-api-php-fpm:${IMAGE_TAG} \
	--file api/docker/production/php-fpm/Dockerfile api

	DOCKER_BUILDKIT=1 docker --log-level=debug build --pull --build-arg BUILDKIT_INLINE_CACHE=1 \
	--target builder \
	--cache-from ${REGISTRY}/gscore-api-php-cli:cache-builder \
	--tag ${REGISTRY}/gscore-api-php-cli:cache-builder \
	--file api/docker/production/php-cli/Dockerfile api

	DOCKER_BUILDKIT=1 docker --log-level=debug build --pull --build-arg BUILDKIT_INLINE_CACHE=1 \
	--cache-from ${REGISTRY}/gscore-api-php-cli:cache-builder \
	--cache-from ${REGISTRY}/gscore-api-php-cli:cache \
	--tag ${REGISTRY}/gscore-api-php-cli:cache \
	--tag ${REGISTRY}/gscore-api-php-cli:${IMAGE_TAG} \
	--file api/docker/production/php-cli/Dockerfile api

try-build:
	REGISTRY=localhost IMAGE_TAG=0 make build

push-build-cache: push-build-cache-gateway push-build-cache-frontend push-build-cache-api

push-build-cache-gateway:
	docker push ${REGISTRY}/gscore-gateway:cache

push-build-cache-frontend:
	docker push ${REGISTRY}/gscore-frontend:cache-builder
	docker push ${REGISTRY}/gscore-frontend:cache

push-build-cache-api:
	docker push ${REGISTRY}/gscore-api:cache
	docker push ${REGISTRY}/gscore-api-php-fpm:cache-builder
	docker push ${REGISTRY}/gscore-api-php-fpm:cache
	docker push ${REGISTRY}/gscore-api-php-cli:cache-builder
	docker push ${REGISTRY}/gscore-api-php-cli:cache

push: push-gateway push-frontend push-api

push-gateway:
	docker push ${REGISTRY}/gscore-gateway:${IMAGE_TAG}

push-frontend:
	docker push ${REGISTRY}/gscore-frontend:${IMAGE_TAG}

push-api:
	docker push ${REGISTRY}/gscore-api:${IMAGE_TAG}
	docker push ${REGISTRY}/gscore-api-php-fpm:${IMAGE_TAG}
	docker push ${REGISTRY}/gscore-api-php-cli:${IMAGE_TAG}

testing-build: testing-build-gateway testing-build-testing-api-php-cli testing-build-cucumber

testing-build-gateway:
	DOCKER_BUILDKIT=1 docker --log-level=debug build --pull --build-arg BUILDKIT_INLINE_CACHE=1 \
	--cache-from ${REGISTRY}/gscore-testing-gateway:cache \
	--tag ${REGISTRY}/gscore-testing-gateway:cache \
	--tag ${REGISTRY}/gscore-testing-gateway:${IMAGE_TAG} \
	--file gateway/docker/testing/nginx/Dockerfile gateway/docker

testing-build-testing-api-php-cli:
	DOCKER_BUILDKIT=1 docker --log-level=debug build --pull --build-arg BUILDKIT_INLINE_CACHE=1 \
	--target builder \
	--cache-from ${REGISTRY}/gscore-testing-api-php-cli:cache-builder \
	--tag ${REGISTRY}/gscore-testing-api-php-cli:cache-builder \
	--file api/docker/testing/php-cli/Dockerfile api

	DOCKER_BUILDKIT=1 docker --log-level=debug build --pull --build-arg BUILDKIT_INLINE_CACHE=1 \
	--cache-from ${REGISTRY}/gscore-testing-api-php-cli:cache-builder \
	--cache-from ${REGISTRY}/gscore-testing-api-php-cli:cache \
	--tag ${REGISTRY}/gscore-testing-api-php-cli:cache \
	--tag ${REGISTRY}/gscore-testing-api-php-cli:${IMAGE_TAG} \
	--file api/docker/testing/php-cli/Dockerfile api

testing-build-cucumber:
	DOCKER_BUILDKIT=1 docker --log-level=debug build --pull --build-arg BUILDKIT_INLINE_CACHE=1 \
	--cache-from ${REGISTRY}/gscore-cucumber-node-cli:cache \
	--tag ${REGISTRY}/gscore-cucumber-node-cli:cache \
	--tag ${REGISTRY}/gscore-cucumber-node-cli:${IMAGE_TAG} \
	--file cucumber/docker/testing/node/Dockerfile \
	cucumber

push-testing-build-cache: push-testing-build-cache-gateway push-testing-build-cache-api-php-cli push-testing-build-cache-cucumber

push-testing-build-cache-gateway:
	docker push ${REGISTRY}/gscore-testing-gateway:cache

push-testing-build-cache-api-php-cli:
	docker push ${REGISTRY}/gscore-testing-api-php-cli:cache-builder
	docker push ${REGISTRY}/gscore-testing-api-php-cli:cache

push-testing-build-cache-cucumber:
	docker push ${REGISTRY}/gscore-cucumber-node-cli:cache

testing-init:
	COMPOSE_PROJECT_NAME=testing docker-compose -f docker-compose-testing.yml up -d
	COMPOSE_PROJECT_NAME=testing docker-compose -f docker-compose-testing.yml run --rm api-php-cli wait-for-it api-postgres:5432 -t 60
	COMPOSE_PROJECT_NAME=testing docker-compose -f docker-compose-testing.yml run --rm api-php-cli php bin/app.php migrations:migrate --no-interaction
	COMPOSE_PROJECT_NAME=testing docker-compose -f docker-compose-testing.yml run --rm testing-api-php-cli php bin/app.php fixtures:load --no-interaction

testing-smoke:
	COMPOSE_PROJECT_NAME=testing docker-compose -f docker-compose-testing.yml run --rm cucumber-node-cli yarn smoke-ci

testing-e2e:
	COMPOSE_PROJECT_NAME=testing docker-compose -f docker-compose-testing.yml run --rm cucumber-node-cli yarn e2e-ci

testing-down-clear:
	COMPOSE_PROJECT_NAME=testing docker-compose -f docker-compose-testing.yml down -v --remove-orphans

try-testing: try-build try-testing-build try-testing-init try-testing-smoke try-testing-e2e try-testing-down-clear

try-testing-build:
	REGISTRY=localhost IMAGE_TAG=0 make testing-build

try-testing-init:
	REGISTRY=localhost IMAGE_TAG=0 make testing-init

try-testing-smoke:
	REGISTRY=localhost IMAGE_TAG=0 make testing-smoke

try-testing-e2e:
	REGISTRY=localhost IMAGE_TAG=0 make testing-e2e

try-testing-down-clear:
	REGISTRY=localhost IMAGE_TAG=0 make testing-down-clear

validate-jenkins:
	curl --user ${USER} -X POST -F "jenkinsfile=<Jenkinsfile" ${HOST}/pipeline-model-converter/validate

deploy:
	ssh -o StrictHostKeyChecking=no deploy@${HOST} -p ${PORT} 'rm -rf site_${BUILD_NUMBER}'
	ssh -o StrictHostKeyChecking=no deploy@${HOST} -p ${PORT} 'mkdir site_${BUILD_NUMBER}'

	envsubst < docker-compose-production.yml > docker-compose-production-env.yml
	scp -o StrictHostKeyChecking=no -P ${PORT} docker-compose-production-env.yml deploy@${HOST}:site_${BUILD_NUMBER}/docker-compose.yml
	rm -f docker-compose-production-env.yml

	ssh -o StrictHostKeyChecking=no deploy@${HOST} -p ${PORT} 'cd site_${BUILD_NUMBER} && docker stack deploy --compose-file docker-compose.yml gscore --with-registry-auth --prune'

deploy-clean:
	rm -f docker-compose-production-env.yml

rollback:
	ssh -o StrictHostKeyChecking=no deploy@${HOST} -p ${PORT} 'cd site_${BUILD_NUMBER} && docker stack deploy --compose-file docker-compose.yml gscore --with-registry-auth --prune'
